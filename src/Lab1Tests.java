import static org.junit.Assert.*;

import org.junit.Test;


public class Lab1Tests {

	/**
	 * Test esistenza classe linked list:
	 * Deve esistere una classe chiamata LinkedList.
	 */
	@Test
	public void test1() {
		try{
			Class.forName("LinkedList");
		}catch(ClassNotFoundException e){
			fail("LinkedList not yet implemented");
		}
	}
	
	/**
	 * Test istanziazione linked list vuota:
	 * Il costruttore di default deve inizializzare una lista vuota.
	 * La descrizione della lista vuota deve essere "[]".
	 */
	@Test
	public void test2() {
		LinkedList l = new LinkedList();
		assertEquals("[]", l.toString());
	}
	
	/**
	 * Test esistenza costruttore linked list da stringa:
	 * Deve esistere un costruttore che prende come parametro un oggetto di tipo String.
	 */
	@Test
	public void test3() {
		try{
			LinkedList.class.getConstructor(java.lang.String.class);
		}catch(NoSuchMethodException e){
			fail("Contructor not yet implemented");
		}
	}
	
	/**
	 * Test istanziazione linked list da stringa:
	 * Il costruttore da String inizializza una lista con gli elementi specificati.
	 * Ogni elemento (numero reale) e' separato da uno spazio.
	 */
	@Test
	public void test4() {
		LinkedList l = new LinkedList("1 2 3");
		assertEquals("[1.0 2.0 3.0]", l.toString());
		l = new LinkedList("1.23 2.001 3.75");
		assertEquals("[1.23 2.001 3.75]", l.toString());
		l = new LinkedList("");
		assertEquals("[]", l.toString());
		
	}
	
	/**
	 * Test istanziazione linked list da stringa malformata:
	 * Se la stringa fornita in argomento al costruttore non e' formattata correttamente,
	 * deve essere sollevata un'eccezione di tipo IllegalArgumentException.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void test4b() {
		new LinkedList("1 2 abb");
	}
	
	/**
	 * Test metodi add:
	 * Devono esistere due metodi addFirst e addLast che accettano entrambi un double come parametro.
	 * addFirst inserisce un nuovo elemento nella prima posizione della lista.
	 * addLast inserisce un nuovo elemento nell'ultima posizione della lista.
	 */
	@Test
	public void test5() {
		try{
			LinkedList.class.getMethod("addFirst", double.class);
			LinkedList.class.getMethod("addLast", double.class);
		}catch(NoSuchMethodException e){
			fail("Method not yet implemented");
		}
		LinkedList l = new LinkedList();
		l.addFirst(1);
		assertEquals("[1.0]", l.toString());
		l.addFirst(3.75);
		assertEquals("[3.75 1.0]", l.toString());
		l.addLast(1.25);
		assertEquals("[3.75 1.0 1.25]", l.toString());
		l = new LinkedList();
		l.addFirst(1);
		LinkedList l2 = new LinkedList();
		l2.addLast(1);
		assertEquals(l, l2);
		
	}

}
