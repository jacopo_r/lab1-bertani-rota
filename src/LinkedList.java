

import java.util.*;
//import java.lang.*;
class Nodo {
    public double data;
    public Nodo nextLink;

  
    public Nodo(double d) {
	    data = d;
    }
    

    
}

class LinkedList {
    private Nodo first;
    private Nodo last;
    public LinkedList(){
    	first = null;
    	last = null;
    	}
    public LinkedList(String s) {
    	if(s.compareTo("") == 1){
    			first = null;
    			last = null;
    		}
    	else{
    		try{
	    	StringTokenizer st = new StringTokenizer(s, " ");
	    	while(st.hasMoreElements()){
	    		addFirst(Double.valueOf((String)st.nextElement()));
	    	}
    		}
	    	catch(Exception e){
	    		throw new IllegalArgumentException(e);
	    		
	    	}
    	
	   }
    }

    public void addFirst(double d) {
    	
	    Nodo link = new Nodo(d);
	    if(first == null){
	    	link.nextLink = first;
	    	last = first;
	    	first = link;
	    }
	    link.nextLink = first;
	    first = link;
    }
    public void addLast(double d){
    	Nodo link = new Nodo(d);
    	link.nextLink = null;
	    link.first = last;
	    }
    public String toString(){
    	String a = "[]";
    	
		if(first == null)
			return a;
		else{
			a = "";
			a = Double.toString(first.data) + a;
			first = first.nextLink;
			while(first != null){
					
					a = Double.toString(first.data) + " " + a;
					first = first.nextLink;
						
				
			}
			a = "[" + a + "]";
			return a;
		}
    }

   

}  
		

